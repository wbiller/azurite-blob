FROM node:12-alpine

RUN mkdir -p /data/blob && \
    npm install -g azurite@3.2.0-preview

ENV BLOB_PORT 10000
ENV BLOB_HOST 0.0.0.0

EXPOSE 10000
VOLUME /data/blob

CMD npx azurite-blob --blobPort ${BLOB_PORT} --blobHost ${BLOB_HOST} -l /data/blob -d /data/blob.log
