# Azurite Blob

A convenient container for Azurite Blob store. Most documentation from https://github.com/azure/azurite applies.


## How to use this image

```bash
docker run -d -p 10000:10000 wbiller/azurite-blob:3.2.0-preview
```

## Environment variables

| Variable  | Default   | Description                        |
|-----------|-----------|------------------------------------|
| BLOB_HOST | `0.0.0.0` | The address to bind to             |
| BLOB_PORT | `10000`     | The port to listen for connections |


## Volumes

### `/data/blob`

This is the workspace directory of the Blob store.
